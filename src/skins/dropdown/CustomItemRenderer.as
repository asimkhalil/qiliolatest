package skins.dropdown
{
	import spark.skins.spark.DefaultItemRenderer;
	
	public class CustomItemRenderer extends DefaultItemRenderer
	{
		public function CustomItemRenderer()
		{
			super();
			this.setStyle("alternatingItemColors",[0xF1F1F1,0xF1F1F1]);
			this.height = 32;
			this.setStyle("color",0x666666);
			this.setStyle("fontSize",13);
		}
	}
}