package utils
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import model.ApplicationModel;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

	public class BookmarkUtil
	{
		public static function saveBookmarks():void {
			var file:File = File.applicationStorageDirectory.resolvePath("bookmark.txt");
			var fileStream:FileStream = new FileStream();
			if(file.exists) {
				file.deleteFile();
			}
			fileStream.open(file,FileMode.WRITE);
			var ids:String = getCommaSeparatedBookmarkIds();
			fileStream.writeUTFBytes(ids);
			fileStream.close();
		}
		
		public static function getBookmarks():void {
			var file:File = File.applicationStorageDirectory.resolvePath("bookmark.txt");
			if(file.exists) {
				var fs:FileStream=new FileStream();
				fs.open(file, FileMode.READ);
				var commaSparatedIds:String = fs.readUTFBytes(fs.bytesAvailable);
				ApplicationModel.bookmarks = new ArrayCollection(commaSparatedIds.split(","));
			}
		}
		
		private static function getCommaSeparatedBookmarkIds():String {
			return ApplicationModel.bookmarks.source.toString();
			/*var ids:String = "";
			for each(var id:String in ApplicationModel.bookmarks) {
				ids += id+",";
			}
			return ids;*/
		}
	}
}