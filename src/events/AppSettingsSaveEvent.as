package events
{
	import flash.events.Event;
	
	public class AppSettingsSaveEvent extends Event
	{
		public static var APP_SETTINGS_SAVE:String = "APP_SETTINGS_SAVE"; 
		
		public var appid:String = "";
		
		public var appsecret:String = "";
		
		public function AppSettingsSaveEvent(type:String, appid:String, appsecret:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.appid = appid;
			this.appsecret = appsecret;
		}
	}
}