package model
{
	import com.jayvenka.fb.FacebookCenter;
	
	import components.CommentAnswersPopUp;
	
	import mx.collections.ArrayCollection;

	public class ApplicationModel
	{
		public static var welcomeTextURL:String = "http://commentmaximiser.com/welcome.html";
		
		public static var commentPopUp:CommentAnswersPopUp;
		
		public static var VERSION_FULL:String = "VERSION_FULL";
		
		public static var VERSION_LIGHT:String = "VERSION_LIGHT";
		
		public static var BATCH_COMMENTS_PAGE_SIZE:int = 20;
		
		public static var SINGLE_USER_LICENSE:String = "singleUserlicense";
		
		public static var MULTI_USER_LICENSE:String = "multiUserlicense";
		
		public static var licenseType:String = SINGLE_USER_LICENSE;
		
		[Bindable]
		public static var filterAdminComments:Boolean = false;
		
		[Bindable]
		public static var facebookCenter:FacebookCenter;
		
		[Bindable]
		public static var selectedCommentIds:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		public static var version:String = VERSION_FULL;
		
		public static var facebookUserProfileId:String = "";
		
		[Bindable]
		public static var userProfilePhoto:String = "";
		
		[Bindable]
		public static var facebookUserName:String = "";
		
		[Bindable]
		public static var selectedReplies:ArrayCollection = new ArrayCollection();
		
		public static var currentIndex:int = 0;
		[Bindable]
		public static var templatesAdded:Boolean = false;
		
		[Bindable]
		public static var appId:String = "";
		
		[Bindable]
		public static var appSecret:String = "";
		 
		public static var createdDate:String = "";
		
		/*[Bindable]
		public static var appId:String = "1441111016127594";
		
		[Bindable]
		public static var appSecret:String = "1dd4ae14049e00aa111907ed464a87cb";*/
		
		public static var LIKED_COMMENTS_LIMIT:int = 10;
		
		public static var commentCountFilter:int = 0;
		
		public static var COMMENTS_BATCH_COUNT:int = 50;
		
		public static var currentLoadedCommentsBatch:int = 0; 
		
		[Bindable]
		public static var batchModelForCommentsLoading:Boolean = false;
		
		public static var batchedLastCommentTime:int=0;
		
		public static var batchCommentOffset:int = 0;
		 
		public static var batchCommentOffsetTarget:int = 0;
		
		[Bindable]
		public static var enableCommentsOwnerThumbnail:Boolean = false;
		
		[Bindable]
		public static var headerHideMode:Boolean = false;
		
		public static var macAddress:String = "";
		
		public static var isLocal:Boolean = false;
		
		[Bindable]
		public static var postsPageSize:int = 10;
		
		[Bindable]
		public static var bookmarksPageSize:int = 13;
		
		[Bindable]
		public static var bookmarks:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		public static var bookmarkedPosts:ArrayCollection = new ArrayCollection();
		
		[Bindable]
		public static var batchLimit:int = 20;
	}
}