package com.pageone.flickr.events
{
	import flash.events.Event;
	
	public class FlickrCallCompleteEvent extends Event
	{
		public function FlickrCallCompleteEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public static const flickrCallComplete:String="flickrCallComplete";
	}
}