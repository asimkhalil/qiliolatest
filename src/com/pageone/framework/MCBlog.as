package com.pageone.framework
{
	import mx.collections.ArrayCollection;
	import mx.utils.StringUtil;

	public class MCBlog
	{
		public function MCBlog()
		{
			categories=new ArrayCollection();
		}
		
		public var url:String;
		public var login:String;
		public var password:String;
		
		public var categories:ArrayCollection; 
		
		public function toXML():String {
			var s:String="<blog>";
			s+=StringUtil.substitute("<url>{0}</url>", escape(this.url));
			s+=StringUtil.substitute("<login>{0}</login>", escape(this.login));
			s+=StringUtil.substitute("<password>{0}</password>", escape(this.password));
			s+="<categories>";
			
			for each(var c:MCCategory in categories) {
				s+=c.toXML();
			}
			
			s+="</categories>";
			s+="</blog>";
			return s;
		}
		
		public static function readXML(x:XML):MCBlog {
			var b:MCBlog=new MCBlog();
			b.url=unescape(x.url);
			b.login=unescape(x.login);
			b.password=unescape(x.password);
			
			for each(var c:XML in x.categories.cat) {
				b.categories.addItem(MCCategory.readXML(c));
			}
			return b;
		}
	}
}