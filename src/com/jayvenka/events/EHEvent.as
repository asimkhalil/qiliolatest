package com.jayvenka.events
{
	import flash.events.Event;
	
	public class EHEvent extends Event
	{
		public var params:Object = {};
		
		public function EHEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,params:Object=null)
		{
			super(type, bubbles, cancelable);
			this.params = params;
		}
	}
}