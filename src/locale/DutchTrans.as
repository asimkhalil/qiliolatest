package locale
{
	public class DutchTrans
	{
		public static var str1:String = "hi <name> thanks.";
		[Bindable]
		public static var str2:String = "hi <name> dank je wel.";
		[Bindable]
		public static var str3:String = "hi <name> bedankt voor de reactie.";
		[Bindable]
		public static var str4:String = "<name> bedankt voor de reactie.";
		[Bindable]
		public static var str5:String = "bedankt voor de reactie.";
		[Bindable]
		public static var str6:String = "hello <name> bedankt voor de reactie";
		[Bindable]
		public static var str7:String = "hello <name> thanks.";
		[Bindable]
		public static var str8:String = "hello <name> dank je wel."; 
	}
}