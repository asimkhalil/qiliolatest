package locale
{
	public class GermanTrans
	{
		public static var str1:String = "Hallo <name> dank.";
		[Bindable]
		public static var str2:String = "Hallo <name> danke.";
		[Bindable]
		public static var str3:String = "Hallo <name> danke für den Kommentar.";
		[Bindable]
		public static var str4:String = "<name> danke für den Kommentar.";
		[Bindable]
		public static var str5:String = "danke für den Kommentar.";
		[Bindable]
		public static var str6:String = "";
		[Bindable]
		public static var str7:String = "";
		[Bindable]
		public static var str8:String = ""; 
	}
}