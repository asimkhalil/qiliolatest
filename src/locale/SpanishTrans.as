package locale
{
	public class SpanishTrans
	{
		public static var str1:String = "hola <name> gracias.";
		[Bindable]
		public static var str2:String = "hola <name> gracias por el comentario.";
		[Bindable]
		public static var str3:String = "<name> gracias por el comentario.";
		[Bindable]
		public static var str4:String = "gracias por el comentario.";
		[Bindable]
		public static var str5:String = "";
		[Bindable]
		public static var str6:String = "";
		[Bindable]
		public static var str7:String = "";
		[Bindable]
		public static var str8:String = ""; 
	}
}