package locale
{
	public class FilipinoTrans
	{
		public static var str1:String = "hi <name> salamat.";
		[Bindable]
		public static var str2:String = "hi <name> salamat sa iyo.";
		[Bindable]
		public static var str3:String = "hi <name> salamat para sa mga komento.";
		[Bindable]
		public static var str4:String = "<name> salamat para sa mga komento.";
		[Bindable]
		public static var str5:String = "<name> salamat para sa mga komento.";
		[Bindable]
		public static var str6:String = "kumusta <name> salamat para sa mga komento";
		[Bindable]
		public static var str7:String = "kumusta <name> salamat.";
		[Bindable]
		public static var str8:String = "kumusta <name> salamat sa iyo."; 
	}
}