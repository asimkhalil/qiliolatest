package locale
{
	public class EnglishTrans
	{
		public static var str1:String = "hi <name> thanks.";
		[Bindable]
		public static var str2:String = "hi <name> thank you.";
		[Bindable]
		public static var str3:String = "hi <name> thanks for the comment.";
		[Bindable]
		public static var str4:String = "<name> thanks for comment.";
		[Bindable]
		public static var str5:String = "thanks for the comment.";
		[Bindable]
		public static var str6:String = "hello <name> thanks for comment";
		[Bindable]
		public static var str7:String = "hello <name> thanks.";
		[Bindable]
		public static var str8:String = "hello <name> thank you."; 
	}
}